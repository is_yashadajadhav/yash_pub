
public class PizzaOrder {
	public displaySizeOrder pizzaSize; //enum object
	public displayTypeOrder pizzaType;

	public displaySizeOrder getpizzaSize() {
		return pizzaSize;
	}

	public void setpizzaSize(displaySizeOrder pizzaSize) {
		this.pizzaSize = pizzaSize;
	}

	public displayTypeOrder getpizzaType() {
		return pizzaType;
	}

	public void setpizzaType(displayTypeOrder pizzaType) {
		this.pizzaType = pizzaType;
	}

}
