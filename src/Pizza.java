import java.util.ArrayList;


public class Pizza {

	public static void main(String[] args) {
		ArrayList<PizzaOrder> pizzaOrderValuesList = new ArrayList<PizzaOrder>();
		InputValidation obj = new InputValidation();
		int quantity = 0;
		int totalQuantity = obj.InputVal(quantity);

		PizzaOrder orderValues = null;
		for (int i = 0; i < totalQuantity; i++) {

			orderValues = new PizzaOrder(); // class n object

			System.out.println("----------------PIZZA " + (i + 1) + "----------------");
			System.out.println("Please provide the size of Pizza : ");
			System.out.println("1.Extra Large | 2.Large | 3.Medium | 4.Small");

			int size = 0;
			int totalSize = obj.InputSize(size);
			while (!(totalSize >= 1) || !(totalSize < 5)) {
				System.out.println("Invalid entry! Please try again..");
				totalSize = obj.InputSize(size);
			}
			orderValues = Util.setpizzaSize(totalSize, orderValues);

			System.out.println("Provide the type of Pizza");
			System.out.println("1.Pan | 2.Cheese Burst");
			int type = 0;
			int totalType = obj.InputType(type);
			while (!(totalType >= 1) || !(totalType < 3)) {
				System.out.println("Invalid entry! Please try again..");
				totalType = obj.InputType(type);
			}
			orderValues = Util.setpizzaType(totalType, orderValues);

			try {
				if ((totalSize == 2) && (totalType == 2))
					throw new Exception(
							"Large pizza with cheese burst is not available. Please try with different options. Thank you!"
									+ "\n");
				else {
					pizzaOrderValuesList.add(orderValues); // Add all values and
															// print what you
															// have ordered
					printOrderFinal.printOrder(pizzaOrderValuesList);
				}
			} catch (Exception my) {
				System.out.println(my.getMessage());
			}
		}

		printOrderFinal.printOrder(pizzaOrderValuesList);
	}

}
