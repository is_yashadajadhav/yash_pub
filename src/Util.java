public class Util {

	public static PizzaOrder setpizzaSize(int totalSize, PizzaOrder orderValues) {
		if (totalSize==1) {
			orderValues.setpizzaSize(displaySizeOrder.EXTRA_LARGE);
		}
		if (totalSize==2){
			orderValues.setpizzaSize(displaySizeOrder.LAGRE);
		}
		if (totalSize==3){
			orderValues.setpizzaSize(displaySizeOrder.MEDIUM);
		}
		if (totalSize==4){
			orderValues.setpizzaSize(displaySizeOrder.SMALL);
		}
		return orderValues;
	}
	
	public static PizzaOrder setpizzaType(int totalType, PizzaOrder orderValues) {
		if (totalType==1){
			orderValues.setpizzaType(displayTypeOrder.PAN);
		}
		if(totalType==2){
			orderValues.setpizzaType(displayTypeOrder.CHEESE_BURST);
		}
		return orderValues;
	}
}