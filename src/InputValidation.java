import java.util.InputMismatchException;
import java.util.Scanner;

public class InputValidation {
	Scanner sc = new Scanner(System.in);
	public int InputVal(int quantity) throws InputMismatchException {
	
		boolean input=false;
		while(!input) {		
		try {
		System.out.println("Enter number of pizza you want :");
		quantity = sc.nextInt();
		input=true;
		} catch (InputMismatchException e) {
			System.out.println("Please enter interger values only.!!");
			sc.next();
		}
	}
		return quantity;
}

	public int InputSize(int pSize) throws InputMismatchException {
		
		boolean input=false;
		while(!input) {		
		try {
		pSize = sc.nextInt();
		input=true;
		} catch (InputMismatchException e) {
			System.out.println("Please enter interger values only.!!");
			sc.next();
		}
	}
		return pSize;
}
	
public int InputType(int type) throws InputMismatchException {
		
		boolean input=false;
		while(!input) {		
		try {
			type = sc.nextInt();
		input=true;
		} catch (InputMismatchException e) {
			System.out.println("Please enter interger values only.!!");
			sc.next();
		}
	}
		return type;
}
}